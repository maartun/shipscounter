package com.martun;

import java.util.ArrayList;

public class ShipsCounter {
    static int[][] ships = {
            /*row1*/{0, 0, 0, 0, 0, 0, 0, 1, 0, 0,},
            /*row2*/{0, 1, 0, 0, 0, 0, 0, 1, 0, 0,},
            /*row3*/{0, 1, 0, 1, 1, 0, 0, 0, 0, 0,},
            /*row4*/{0, 1, 0, 1, 1, 0, 0, 1, 1, 1,},
            /*row5*/{0, 1, 0, 0, 0, 0, 0, 0, 0, 0,},
            /*row6*/{0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            /*row7*/{1, 1, 1, 1, 0, 1, 0, 0, 0, 0,},
            /*row8*/{0, 0, 0, 0, 0, 0, 0, 0, 0, 0,},
            /*row9*/{1, 1, 0, 0, 0, 0, 0, 1, 0, 0,},
            /*row10*/{1, 1, 0, 0, 0, 0, 0, 0, 0, 1,}
    };

    public static void main(String[] args) {
        countTheShips(ships);
    }

    public static void countTheShips(int[][] arr) {
        ArrayList<Coordinate> busyCoordinates = new ArrayList<>();
        int n = arr.length;
        int m = 10;
        int count = 0;
        for (int y = 0; y < n; y++) {
            outerLoop:
            for (int x = 0; x < m; x++) {
                if (arr[y][x] == 1) {
                    for (Coordinate busyCoordinate : busyCoordinates) {
                        if (busyCoordinate.x == x && busyCoordinate.y == y) {
                            continue outerLoop;
                        }
                    }
                    count++;
                    if (x < arr.length - 1 && arr[y][x + 1] == 1) {
                        int i = x + 1;
                        while (i < arr.length && arr[y][i] == 1) {
                            busyCoordinates.add(new Coordinate(y, i));
                            i++;
                        }
                    } else if (y < m - 1 && arr[y + 1][x] == 1) {
                        int i = y + 1;
                        while (i < m && arr[i][x] == 1) {
                            busyCoordinates.add(new Coordinate(i, x));
                            i++;
                        }
                    }
                }
            }
        }
        System.out.println("Ships counted " + count);
    }
}